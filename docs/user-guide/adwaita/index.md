# Adwaita

Adwaita (also known as Libadwaita or Adw) is a set of additional widgets and classes that supplement the GTK toolkit,
which is the default user interface toolkit for the GNOME desktop environment. Adwaita is designed to implement the
standard GNOME design patterns as documented in
the [GNOME Human Interface Guidelines](https://developer.gnome.org/hig/) (HIG). The HIG provides guidelines and
recommendations for creating user interfaces that are consistent, intuitive, and accessible across all GNOME
applications.

Adwaita provides a set of widgets and classes that are specifically designed to match the GNOME HIG, including buttons,
switches, and dialogs. These widgets are designed to provide a consistent and modern look and feel that adheres to the
GNOME design patterns. Adwaita also includes support for high-resolution displays and provides scalable icons that can
be used across different display sizes.

In addition to its widgets and classes, Adwaita also provides CSS stylesheets that define the visual appearance of the
widgets. These stylesheets provide a convenient and flexible way to customize the appearance of the widgets while still
adhering to the GNOME HIG. Adwaita also provides support for dark mode, which can be toggled system-wide or on a
per-application basis.

Overall, Adwaita is an essential component of the GNOME desktop environment, providing a consistent and modern look and
feel for GNOME applications. Its adherence to the GNOME HIG ensures that applications built with Adwaita have a
consistent user interface, making it easier for users to navigate and use the applications. Its support for
high-resolution displays and dark mode makes it a versatile and adaptable toolkit for building modern desktop
applications.
